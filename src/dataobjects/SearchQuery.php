<?php

namespace Hestec\BingCustomSearch;

use SilverStripe\ORM\DataObject;

class SearchQuery extends DataObject {

    private static $table_name = 'BingCustomSearchQuery';

    private static $db = [
        'Query' => 'Text',
        'Ip' => 'Varchar(50)',
        'Config' => 'Int'
    ];

}