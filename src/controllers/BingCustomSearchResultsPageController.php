<?php

namespace Hestec\BingCustomSearch;

use GuzzleHttp\Client;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Convert;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataObject;

class BingCustomSearchResultsPageController extends \PageController {

    private static $allowed_actions = array (
    );

    public function init(){
        parent::init();

    }

    public function getResults(){

        if (isset($_GET['c']) && isset($_GET['s']) && $config = Config::inst()->get('BingCustomSearch')){

            $confignr = $_GET['c'];

            $ApiKey = $config['AccessKey_'.$confignr];
            $CustomConfig = $config['CustomConfig_'.$confignr];
            $Market = $config['Market_'.$confignr];
            $saveQuery = Convert::raw2sql(trim($_GET['s']));
            unset($decodedresponse);
            $results = ArrayList::create();

            $q = new SearchQuery();
            $q->Ip = $_SERVER['REMOTE_ADDR'];
            $q->Query = $_GET['s'];
            $q->Config = $_GET['c'];
            $q->write();

            if(strlen($saveQuery))
            {
                $client = new Client([
                    // Base URI is used with relative requests
                    'base_uri' => 'https://api.bing.microsoft.com',
                    'timeout'  => 2.0
                ]);

                $response = $client->request('GET','/v7.0/custom/search', [
                    // 'debug' => true,
                    'headers' => [
                        'Ocp-Apim-Subscription-Key' => $ApiKey
                    ],
                    'query' => [
                        'q' => $saveQuery,
                        'customconfig' => $CustomConfig,
                        'mkt' => $Market
                    ]
                ]);
                $decodedresponse = json_decode($response->getBody(), true);
            }

            if (isset($decodedresponse['webPages']))
            {
                foreach ($decodedresponse['webPages']['value'] as $result)
                {
                    $m = DataObject::create();

                    if (isset($result['id'])) {
                        $m->ID = $result['id'];
                    }
                    if (isset($result['url'])) {
                        $m->URL = $result['url'];
                    }
                    if (isset($result['name'])) {
                        $m->Name = $result['name'];
                    }
                    if (isset($result['snippet'])) {
                        $m->Description = $result['snippet'];
                    }

                    $results->push($m);
                }
            }

            return $results;

        }

    }

    public function SearchTerm(){

        if (isset($_GET['s']) && strlen($_GET['s']) > 0){

            return $_GET['s'];

        }
        return false;

    }

}