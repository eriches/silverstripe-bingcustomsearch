<?php

namespace Hestec\BingCustomSearch;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Versioned\Versioned;

class BingCustomSearchResultsPage extends \Page {

    public function requireDefaultRecords() {
        parent::requireDefaultRecords();
        if (SiteTree::get()->filter(Array('ClassName' => __CLASS__))->count() == 0) {
            $page = BingCustomSearchResultsPage::create();
            $page->Title = 'Search Results';
            $page->Content= '';
            $page->URLSegment = 's';
            $page->write();
            $page->copyVersionToStage(Versioned::DRAFT, Versioned::LIVE);
            $page->flushCache();
        }
    }

}