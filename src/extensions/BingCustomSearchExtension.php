<?php

namespace Hestec\BingCustomSearch;

use SilverStripe\Core\Config\Config;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\RequiredFields;

class BingCustomSearchExtension extends DataExtension {

    private static $allowed_actions = array(
        'BingCustomSearchForm'
    );

    public function LinkToBingCustomSearchResultsPage() {
        if ($link = SiteTree::get()->filter(Array('ClassName' => 'Hestec\BingCustomSearch\BingCustomSearchResultsPage'))->first()) {
            return $link->AbsoluteLink();
        }
    }

    public function BingCustomSearchForm($confignr = 1, $placeholder = false)
    {

        $SearchField = TextField::create('Search', '');
        $SearchField->setFieldHolderTemplate("BingCustomSearchFormField_holder");
        if ($placeholder){
            $SearchField->setAttribute('placeholder', $placeholder);
        }

        $ConfigField = HiddenField::create('Config', 'Config', $confignr);

        $Action = FormAction::create('SubmitBingCustomSearchForm', _t("BingCustomSearch.SUBMIT", "Search"));

        $fields = FieldList::create(array(
            $SearchField,
            $ConfigField
        ));

        $actions = FieldList::create(
            $Action
        );

        $validator = new RequiredFields([
            'Search'
        ]);

        $form = Form::create($this->owner, __FUNCTION__, $fields, $actions, $validator);
        $form->setTemplate('BingCustomSearchForm');
        //$form->setAttribute('up-target', '.floating-content .search .txt');

        $form->setFormMethod('GET')
            ->disableSecurityToken();
        if (isset($_GET['c']) && isset($_GET['s'])) {
            $form->loadDataFrom(array(
                'Config' => $_GET['c'],
                'Search' => $_GET['s']
            ));
        };

        return $form;
    }

    public function SubmitBingCustomSearchForm($data,$form)
    {

        $output = "?c=".$data['Config'];
        $output .= "&s=".$data['Search'];

        return $this->owner->redirect($this->LinkToBingCustomSearchResultsPage().$output);

    }

}
